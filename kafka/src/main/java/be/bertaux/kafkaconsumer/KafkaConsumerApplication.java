package be.bertaux.kafkaconsumer;

import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;

@SpringBootApplication
public class KafkaConsumerApplication {
	private static final Logger LOG = LoggerFactory.getLogger(KafkaConsumerApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(KafkaConsumerApplication.class, args);
	}

	@Bean
	public Consumer<Message<Person>> input() {
		return message -> LOG.info("Person '{}'", message.getPayload());
	}

}
