package be.bertaux.kafkastreamsprocessor;

import java.util.function.Function;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class KafkaStreamsProcessorApplication {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaStreamsProcessorApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(KafkaStreamsProcessorApplication.class, args);
    }

    @Bean
    public Function<KStream<String, String>, KStream<String, Person>> processPerson() {
        return input -> input
                .map((key, value) -> {
                    LOG.info("Message key '{}' payload '{}'", key, value);
                    return new KeyValue<>(key, new Person(value, 42));
                });
    }

}
